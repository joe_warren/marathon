#!/usr/bin/env python3
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
import shlex
import subprocess

class Marathon:
    def delete_event(self, widget, event, data=None):
        Gtk.main_quit()
        return False

    def enter_callback(self, widget, entry):
        entry_text = entry.get_text()
        print( "Entry contents: %s\n" % entry_text )

    def edit_callback(self, widget, newtext, length, pos, entry):
        entry_text = entry.get_text()
        print( "Entry contents: %s\n" % entry_text )
        print( "New Text: %s\n" % newtext )

    def changed_callback(self, widget, entry):
        entry_text = entry.get_text()
        print( "New Text: %s\n" % entry_text )

    def autocomplete(self, text):
        event = subprocess.Popen(
            "compgen -c " + shlex.quote(text),
            shell=True, 
            stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            close_fds=True,
            executable="/bin/bash")
        out = event.stdout.readlines()
        return [o.strip() for o in out]

    def key_callback(self, widget, key, entry):
        if(key.keyval == Gdk.KEY_Tab):
            print( self.autocomplete(entry.get_text()) )
            return True
        return False

    def __init__(self):
        self.window = Gtk.Window()

        self.window.set_title("Marathon Runner")

        self.window.connect("delete_event", self.delete_event)

        self.window.set_border_width(10)

        self.box1 = Gtk.HBox(False, 0)

        self.window.add(self.box1)
  
        self.entry = Gtk.Entry()
        self.entry.connect("activate", self.enter_callback, self.entry)
        self.entry.connect("changed", self.changed_callback, self.entry)
        self.entry.connect("key-press-event", self.key_callback, self.entry)

        self.box1.pack_start(self.entry, True, True, 0)

        self.entry.show()
        self.box1.show()
        self.window.show()

def main():
    Gtk.main()

if __name__ == "__main__":
    Marathon()
    main()
